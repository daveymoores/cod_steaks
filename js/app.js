// Foundation JavaScript
// Documentation can be found at: http://foundation.zurb.com/docs
$(document).foundation();

$(document).ready(function(){


	/*--------------slick carousel init and events-----------------*/

	$('.carousel').slick({
		  dots: true,
		  infinite: true,
		  arrows: false,
		  slidesToShow: 1,
		  slidesToScroll: 1,
		  autoplay: true,
  		  autoplaySpeed: 2000,
  		  speed: 800,
		  fade: true,
		  cssEase: 'linear',
		onAfterChange: function(el){
			var imgCopy = $('.slick-active').find('img').attr('alt');
			console.log(imgCopy);
			$('#slideCopy').text(imgCopy);
		},
		onInit: function(el){
			var imgCopy = $('.slick-active').find('img').attr('alt');
			console.log(imgCopy);
			$('.slick-list').after('<div id="slideCopy">'+ imgCopy +'</div>');
		},
		  responsive: [
		    {
		      breakpoint: 1024,
		      settings: {
		        slidesToShow: 1,
		        slidesToScroll: 1
		      }
		    },
		    {
		      breakpoint: 600,
		      settings: {
		        slidesToShow: 1,
		        slidesToScroll: 1
		      }
		    },
		    {
		      breakpoint: 480,
		      settings: {
		        slidesToShow: 1,
		        slidesToScroll: 1
		      }
		    }

		  ]
		});




	/*--------------end slick-----------------*/


	/*--------hover states---------*/

	$('.cod_project-wrapper').on('mouseover', function(){

		$el = $(this);
		$el.addClass('active');

	});

	$('.cod_project-wrapper').on('mouseout', function(){

		$el = $(this);
		$el.removeClass('active');

	});

	/*-------end hover state-------*/

	$('.dropdown').on('mouseover', 'li', function(){

		$el = $(this);
		$el.addClass('active');

	});

});
